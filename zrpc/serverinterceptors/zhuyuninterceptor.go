package serverinterceptors

import (
	"context"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func ZhuyunInterceptor(ctx context.Context, req any,
	info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		md = metadata.MD{}
	}

	newCtx := ZhuyunZRPCExtract(ctx, nil, &md)

	resp, err := handler(newCtx, req)

	return resp, err
}

func ZhuyunZRPCExtract(ctx context.Context, p propagation.TextMapPropagator, metadata *metadata.MD) context.Context {
	values := metadata.Get("traceState")
	if len(values) == 0 {
		return ctx
	}

	stateStr := values[0]
	if len(stateStr) == 0 {
		return ctx
	}

	state, _ := trace.ParseTraceState(stateStr)

	return context.WithValue(ctx, "ctxKey", state)
}
