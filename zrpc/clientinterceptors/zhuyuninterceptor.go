package clientinterceptors

import (
	"context"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

/*
	zhuyunMetadata
	pkgName string
*/

func ZhuyunInterceptor(ctx context.Context, method string, req, reply interface{},
	cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {

	md, ok := metadata.FromOutgoingContext(ctx)
	if !ok {
		md = metadata.MD{}
	}

	ZhuyunZRPCInject(ctx, nil, &md)

	ctx = metadata.NewOutgoingContext(ctx, md)

	err := invoker(ctx, method, req, reply, cc, opts...)

	return err
}

func ZhuyunZRPCInject(ctx context.Context, p propagation.TextMapPropagator, metadata *metadata.MD) {
	if ctx == nil {
		return
	}
	zhuyunState, ok := ctx.Value("zhuyunStateCtxKey").(*trace.TraceState)
	if !ok {
		return
	}
	metadata.Set("zhuyunState", zhuyunState.String())
}
