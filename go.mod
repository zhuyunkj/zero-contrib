module gitee.com/zhuyunkj/zero-contrib

go 1.16

require (
	github.com/nacos-group/nacos-sdk-go/v2 v2.1.0
	github.com/pkg/errors v0.9.1
	github.com/zeromicro/go-zero v1.4.0
	google.golang.org/grpc v1.49.0
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/stretchr/testify v1.8.0
	go.opentelemetry.io/otel v1.9.0
	go.opentelemetry.io/otel/trace v1.9.0
)
