package handler

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

//func TestPromMetricHandler_Disabled(t *testing.T) {
//	promMetricHandler := PrometheusHandler("/user/login", http.MethodGet)
//	handler := promMetricHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		w.WriteHeader(http.StatusOK)
//	}))
//
//	req := httptest.NewRequest(http.MethodGet, "http://localhost", http.NoBody)
//	resp := httptest.NewRecorder()
//	handler.ServeHTTP(resp, req)
//	assert.Equal(t, http.StatusOK, resp.Code)
//}
//
//func TestPromMetricHandler_Enabled(t *testing.T) {
//	prometheus.StartAgent(prometheus.Config{
//		Host: "localhost",
//		Path: "/",
//	})
//	promMetricHandler := PrometheusHandler("/user/login", http.MethodGet)
//	handler := promMetricHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		w.WriteHeader(http.StatusOK)
//	}))
//
//	req := httptest.NewRequest(http.MethodGet, "http://localhost", http.NoBody)
//	resp := httptest.NewRecorder()
//	handler.ServeHTTP(resp, req)
//	assert.Equal(t, http.StatusOK, resp.Code)
//}

func TestPrometheusHandlerFunc(t *testing.T) {
	handler := PrometheusHandlerFunc("")

	tests := []struct {
		name     string
		req      *http.Request
		recorder *httptest.ResponseRecorder
	}{
		{
			name:     "",
			req:      httptest.NewRequest(http.MethodGet, "/api/content/popup/buyVideo?videoId=123123", nil),
			recorder: httptest.NewRecorder(),
		},
		{
			name:     "",
			req:      httptest.NewRequest(http.MethodPost, "/api/content/popup/buyVideo?videoId=123123", strings.NewReader("ping")),
			recorder: httptest.NewRecorder(),
		},
		{
			name:     "",
			req:      httptest.NewRequest(http.MethodPost, "/api/content/popup/buyVideo", strings.NewReader("ping")),
			recorder: httptest.NewRecorder(),
		},
		{
			name:     "",
			req:      httptest.NewRequest(http.MethodPost, "/", strings.NewReader("ping")),
			recorder: httptest.NewRecorder(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler(func(writer http.ResponseWriter, request *http.Request) {
				_, _ = writer.Write([]byte("ok"))
				writer.WriteHeader(http.StatusOK)
			})(tt.recorder, tt.req)
			assert.Equal(t, http.StatusOK, tt.recorder.Code)
		})
	}

}
