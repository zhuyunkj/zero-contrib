package handler

import (
	"github.com/zeromicro/go-zero/core/metric"
	"github.com/zeromicro/go-zero/core/timex"
	"net/http"
	"strconv"
)

const serverNamespace = "go_http_server" // 区分 http_server

var (
	metricServerReqDur = metric.NewHistogramVec(&metric.HistogramVecOpts{
		Namespace: serverNamespace,
		Subsystem: "requests",
		Name:      "duration_ms",
		Help:      "http server requests duration(ms).",
		Labels:    []string{"path", "method", "code", "pkgName"},
		Buckets:   []float64{5, 10, 25, 50, 100, 250, 500, 750, 1000},
	})

	metricServerReqCodeTotal = metric.NewCounterVec(&metric.CounterVecOpts{
		Namespace: serverNamespace,
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "http server requests error count.",
		Labels:    []string{"path", "method", "code", "pkgName", "bizCode"},
	})
)

// PrometheusHandlerFunc returns a middleware that reports stats to prometheus.
func PrometheusHandlerFunc(pkgNameHeader string, getBizCodeFunc GetBizCodeFunc) func(http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			startTime := timex.Now()
			cw := NewWithCodeResponseWriter(w, getBizCodeFunc)

			next.ServeHTTP(cw, r)

			code := strconv.Itoa(cw.Code)
			pkgName := r.Header.Get(pkgNameHeader)
			bizCode := strconv.Itoa(cw.BizCode)
			path, method := r.URL.Path, r.Method

			metricServerReqDur.Observe(timex.Since(startTime).Milliseconds(), path, method, code, pkgName)
			metricServerReqCodeTotal.Inc(path, method, code, pkgName, bizCode)
		}
	}
}
